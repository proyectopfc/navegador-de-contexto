package com.navegador.gestortareas.tareas;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
/**
 * Clase que inicializa y obtiene el estado del sensor de proximidad.
 * @author Guillermo
 *
 */
public class SensorProximidad extends MiSensor{
	
	
	private Sensor proximidad;

	public SensorProximidad(SensorManager sensorManager) {
		super(sensorManager);
		//Inicializa el sensor especifico de proximidad
		proximidad = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
		//Se registra el sensor, si no existe lanza una excepci�n
		this.registrarSensor(proximidad); 
	}

	/**
	 * M�todo que registra las mediciones del sensor cuando cambia. 
	 * El sensor de proximidad registra un valor que son los centimetros
	 *  de distancia entre el sensor y el objeto mas pr�ximo.
	 *  El sensor esta limitado por su valor m�ximo de medici�n.
	 */
	@Override
	public void onSensorChanged(SensorEvent arg0) {
		float[] masData;
		switch(arg0.sensor.getType()){
			case Sensor.TYPE_PROXIMITY:
				masData = arg0.values;
				x = masData[0];
				break;
		}
	}

}
