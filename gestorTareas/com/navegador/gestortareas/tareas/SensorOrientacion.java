package com.navegador.gestortareas.tareas;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;

/**
 * Clase que inicializa y obtiene el estado del sensor de orientación.
 * 
 * @author Guillermo
 *
 */
public class SensorOrientacion extends MiSensor{
	
	private Sensor orientacion;
	
	public SensorOrientacion(SensorManager sensorManager){
		super(sensorManager);
		//Inicializa el sensor especifico de orientación
		this.orientacion = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION); 
		//Se registra el sensor
		this.registrarSensor(orientacion); 
		
	}
	
	/**
	 * Método que registra todas las mediciones del sensor cada vez que existe un cambio
	 */
	public void onSensorChanged(SensorEvent arg0) {
		synchronized (this){
			float[] masData;
			switch(arg0.sensor.getType()){
	             case Sensor.TYPE_ORIENTATION:
	            	 		masData = arg0.values;
	            	 		this.x = masData[0];
	            	 		this.y = masData[1];
	            	 		this.z = masData[2];
					break;
				default:
					break;
				}
			}
	}
}
