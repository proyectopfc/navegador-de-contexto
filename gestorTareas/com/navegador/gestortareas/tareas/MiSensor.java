package com.navegador.gestortareas.tareas;

import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 *  Clase abstracta donde se implementan m�todos generales
 *  de los sensores del dispositivo.
 * 
 * @author Guillermo
 */
public abstract class MiSensor extends DatosContexto implements SensorEventListener  {

	public SensorManager sensorManager;//
	
	/**
	 * Contructor
	 * El par�metro sensorManager se obtiene con el m�todo ((SensorManager) getSystemService(SENSOR_SERVICE)) 
	 * en la actividad que este activa en la aplicaci�n.
	 * 
	 * @param sensorManager
	 */
	public MiSensor(SensorManager sensorManager){
		this.sensorManager = sensorManager;
	}
	
	public void onAccuracyChanged(Sensor arg0, int arg1) {
		// TODO Auto-generated method stub
	}
	
	/**
	 * M�todo que registra un sensor en el SensorManager.
	 *  Una vez registrado el sensor, el sensor esta activo y toma los valores de contexto peri�dicamente.
	 * 
	 * @param sensor
	 */
	public void registrarSensor(Sensor sensor){
		if(sensor == null){
   		//No hay sensor
		}
		else{
   		sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
		}	
	}
	
	/**
	 * M�todo que des-regitra el sensor registrado en esta clase. 
	 * Este m�todo es recomendable para disminuir el gasto de la bateria.
	 * 
	 */
	public void desRegistrar(){
		this.sensorManager.unregisterListener(this);
	}
}
