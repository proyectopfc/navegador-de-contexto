package com.navegador.gestortareas.tareas;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
/**
 * Clase que inicializa y obtiene el estado del sensor de acelerometro.
 * 
 * @author Guillermo
 *
 */
public class SensorAcelerometro extends MiSensor{
	
	private Sensor acelerometro;
	
	public SensorAcelerometro(SensorManager sensorManager) {
		super(sensorManager);
		//Inicializa el sensor especifico del acelerometro
		this.acelerometro = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		//Se registra el sensor
		this.registrarSensor(this.acelerometro); 
	}
	
	/**
	 * M�todo que registra todas las mediciones del sensor cada vez que existe un cambio
	 */
	public void onSensorChanged(SensorEvent arg0) {
		synchronized (this){
			float[] masData;
			switch(arg0.sensor.getType()){
	             case Sensor.TYPE_ACCELEROMETER:
	            	 		masData = arg0.values;
	            	 		this.x = masData[0];
	            	 		this.y = masData[1];
	            	 		this.z = masData[2];
					break;
				default:
					break;
				}
			}
	}
}
