package com.navegador.gestortareas.tareas;

import com.navegador.gestortareas.lanzador.IdParametros;
import com.navegador.gestortareas.respuestas.Respuesta;

import android.app.Activity;
import android.util.Log;
/**
 * Clase abstracta para las tareas que hacen uso de una actividad: c�digoQr, c�mara.
 *  Estas actividades realizan la respuesta. 
 * 
 * @author Guillermo
 *
 */
public abstract class ActividadTarea extends Activity{
	
	protected String DSK;
	protected String reciever;
	
	protected Respuesta respuesta;
	
	
	/**
	 * M�todo que a�ade los par�metros al objeto respuesta y realiza una petici�n al servidor.
	 * 
	 * @param cadena
	 */
	public void enviarRespuesta(String cadena){//assert reciever!=null
		respuesta = new Respuesta(reciever);
		respuesta.a�adir(IdParametros.DSK,DSK); 
		respuesta.a�adir(IdParametros.cadena,cadena);// cadena de texto con la respuesta codificada 
		Log.i("Respuesta petici�n",respuesta.getPeticion());// se realiza la petici�n 
		
	}

	/**
	 * M�todo que inicializa el objeto Respuesta y realiza una petici�n para el envio de un fichero.
	 * 
	 * @param path
	 * @param DSK
	 */
	public void enviarFile(String path,String DSK){
		respuesta = new Respuesta(reciever);
		//respuesta.add(IdParametros.DSK,DSK);
		respuesta.enviarFile(path,DSK);
	}
}
