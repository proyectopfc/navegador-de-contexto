package com.navegador.gestortareas.tareas.complejas.oVirtual;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;

import com.navegador.gestortareas.lanzador.IdParametros;
import com.navegador.gestortareas.tareas.complejas.oVirtual.graficos.LightingRenderer;
import com.navegador.gestortareas.tareas.complejas.oVirtual.graficos.Model3D;
import com.navegador.gestortareas.tareas.complejas.oVirtual.modelos.Model;
import com.navegador.gestortareas.tareas.complejas.oVirtual.parser.ObjParser;
import com.navegador.gestortareas.tareas.complejas.oVirtual.parser.ParseException;
import com.navegador.gestortareas.tareas.complejas.oVirtual.parser.Util;
import com.navegador.gestortareas.tareas.complejas.oVirtual.util.AssetsFileUtil;
import com.navegador.gestortareas.tareas.complejas.oVirtual.util.BaseFileUtil;
import com.navegador.gestortareas.tareas.complejas.oVirtual.util.SDCardFileUtil;

import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import edu.dhbw.andar.ARToolkit;
import edu.dhbw.andar.AndARActivity;
import edu.dhbw.andar.exceptions.AndARException;

/**
 * Actividad que hereda de AndARActivity
 * Se obtiene el fichero obj y se registra para representr el modelo del fichero obj
 *  en la vista del dispositivo.
 * 
 */
public class CustomActivity extends AndARActivity {

	/**
	 * Fichero obj en la carpeta assets
	 */
	public static final int TYPE_INTERNAL = 0;
	/**
	 * Fichero obj en la memoria SD del dispositivo
	 */
	public static final int TYPE_EXTERNAL = 1;
	
	ARToolkit artoolkit;
	private Model model;
	private Model3D model3d;
	private ProgressDialog waitDialog;
	private Resources res;// Tipo interno de memoria para obtener el fichero obj
	
	private String marca;//tipo de marca codificada en el XML
	private String url;//url codificado en el XML donde indica la direcci�n web para descargar obj
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		//Obtener datos del intent
		Bundle bundle = getIntent().getExtras();
		this.marca = bundle.getString(IdParametros.marca);
		this.url = bundle.getString(IdParametros.url);
		
		super.setNonARRenderer(new LightingRenderer());//or might be omited
		res=getResources();
		artoolkit = getArtoolkit();		
		getSurfaceView().getHolder().addCallback(this);
		
		

	}

	public void uncaughtException(Thread thread, Throwable ex) {
		Log.e("AndAR EXCEPTION", ex.getMessage());
		finish();
	}

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    	super.surfaceCreated(holder);
    	//load the model
    	//this is done here, to assure the surface was already created, so that the preview can be started
    	//after loading the model
    	if(model == null) {
			waitDialog = ProgressDialog.show(this, "", 
	                "Loading", true);
			waitDialog.show();
			new ModelLoader().execute();
		}
    }
    
    private String getNombreModelo(){
    	String nombre = "";
    	if(url!=null && url.length()>8){
    		String path[] = url.split("/");
    		nombre = path[path.length-1];
    	}
    	return nombre;
    }
    
private class ModelLoader extends AsyncTask<Void, Void, Void> {
		
		
    	@Override
    	protected Void doInBackground(Void... params) {
    		
    		int type = 1;//Tipo de memoria externa
    		String modelFileName = getNombreModelo();
    		//Se obtiene el directorio de la aplicaci�n donde se almacen ficheros de la aplicaci�n,
    		//en concretro la carpeta exclusiva de im�genes.
    		String extFiles = getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath();
    		Log.d("CustomActivity", extFiles);
    		//extFiles = extFiles.substring(4); // en algunos casos hay que eliminar la primera parte de la direcci�n para poder acceder
    		//Log.d("CustomActivity", extFiles);
    		//se define la ruta final del modelo obj en el dispositivo
    		modelFileName = "file:///"+extFiles+"/"+"download"+"/"+modelFileName;
			BaseFileUtil fileUtil= null;
			File modelFile=null;
			switch(type) {
			case TYPE_EXTERNAL:
				fileUtil = new SDCardFileUtil();
				modelFile =  new File(URI.create(modelFileName));
				
				//en el caso de que el par�metro url "exista" se descarga el modelo de la direcci�n url
				if (url != null && url.length() > 7)
					Log.d("CustomActivity", String.valueOf(downloadModel (modelFile.getAbsolutePath(), url)));
				
				modelFileName = modelFile.getName();
				fileUtil.setBaseFolder(modelFile.getParentFile().getAbsolutePath());
				break;
			case TYPE_INTERNAL:
				fileUtil = new AssetsFileUtil(getResources().getAssets());
				fileUtil.setBaseFolder("models/");
				break;
			}
			
			//se lee el modelo del fichero obj						
			if(modelFileName.endsWith(".obj")) {
				ObjParser parser = new ObjParser(fileUtil);
				try {
					if(type == TYPE_EXTERNAL) {
						//un fichero externo se debe recortar
						BufferedReader modelFileReader = new BufferedReader(new FileReader(modelFile));
						String shebang = modelFileReader.readLine();				
						if(!shebang.equals("#trimmed")) {
							//recortar el fichero:			
							File trimmedFile = new File(modelFile.getAbsolutePath()+".tmp");
							BufferedWriter trimmedFileWriter = new BufferedWriter(new FileWriter(trimmedFile));
							Util.trim(modelFileReader, trimmedFileWriter);
							if(modelFile.delete()) {
								trimmedFile.renameTo(modelFile);
							}					
						}
					}
					if(fileUtil != null) {
						BufferedReader fileReader = fileUtil.getReaderFromName(modelFileName);
						if(fileReader != null) {
							model = parser.parse("Model", fileReader);
							model3d = new Model3D(model);
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
    		return null;
    	}
    	@Override
    	protected void onPostExecute(Void result) {
    		super.onPostExecute(result);
    		waitDialog.dismiss();
    		
    		//se registra el modelo
    		try {
    			if(model3d!=null)
    				artoolkit.registerARObject(model3d);
			} catch (AndARException e) {
				e.printStackTrace();
			}
			startPreview();
    	}
    	
    	private boolean  downloadModel (String fileName, String uri) {
			String modelName = fileName;
			String URIName = uri;
			if (FileUtil.downloadFile (modelName, URIName, true)) {  // get the obj file
				modelName = modelName.substring(0, modelName.length() - 4) + ".mtl";
				URIName = URIName.substring(0, URIName.length() - 4) + ".mtl";
				if (FileUtil.downloadFile (modelName, URIName, true)) {
					// se intenta descargar el modelo en los formatos: png, gif, y jpg
					//  y si no existen no pasa nada.
					modelName = modelName.substring(0, modelName.length() - 4) + ".png";
					URIName = URIName.substring(0, URIName.length() - 4) + ".png";
					FileUtil.downloadFile (modelName, URIName, false);
					modelName = modelName.substring(0, modelName.length() - 4) + ".jpg";
					URIName = URIName.substring(0, URIName.length() - 4) + ".jpg";
					FileUtil.downloadFile (modelName, URIName, false);
					modelName = modelName.substring(0, modelName.length() - 4) + ".gif";
					URIName = URIName.substring(0, URIName.length() - 4) + ".gif";
					FileUtil.downloadFile (modelName, URIName, false);
					return true;
				}
			}
			return false;

		}
    }
	
}
