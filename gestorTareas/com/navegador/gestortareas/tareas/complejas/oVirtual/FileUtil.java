package com.navegador.gestortareas.tareas.complejas.oVirtual;

import java.io.BufferedInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.util.Log;

public class FileUtil {

	private static final String TAG = "FileUtil";	

	/**
	 *   
	 *  Esta clase implementa la funci�n de descargar un archivo .obj de un servicion web
	 *  y almacena el fichero en el dispositivo. 
	 *   
	 * @param filename
	 *            Path completo del fichero
	 * @param uri
	 *            URL para obtener el fichero de la web
	 * @return true si se ha guardado el fichero correctamente o si ya existia el fichero previamente.

	 */
	@SuppressWarnings("finally")
	public  static boolean downloadFile (String fileName, String uri, boolean report) {
		File file = new File (fileName);
		if (file.exists())
			return true;
		boolean result = false;
		OutputStream  outStream = null;
		URL url = null;
		InputStream input = null;
		Log.d(TAG, "downloading url:" + uri);

		try {

			url = new URL (uri);
			URLConnection conexion = url.openConnection();
			conexion.connect();
			input = new BufferedInputStream(url.openStream());

			//  Create the directory if necessary
			File directory = new File(pathComponent(fileName));
			if (!directory.exists()) 
				directory.mkdirs();

			outStream = new FileOutputStream(file, true);
			byte[] buffer = new byte[1024];
			int readSize = 0;
			while ((readSize = input.read(buffer, 0, buffer.length)) != -1) {
				outStream.write(buffer, 0, readSize);
			}
			result = true;

		} catch (IOException e) {
			if (report)
				Log.e(TAG + "Unable to download model file", fileName + uri + e);
			result = false;
		} finally {
			close (outStream);
			close (input);
			return result;	
		}
	}
	/**
	 * 
	 * Borrar informaci�n del path de un fichero y obtiene s�lo su fichero componente.
	 * 
	 * @param filename
	 *            el fichero

	 */
	public static String fileComponent(String filename) {
		int i = filename.lastIndexOf(File.separator);
		return (i > -1) ? filename.substring(i + 1) : filename;
	}
	/**
	 * 
	 * Borrar informaci�n del path de un fichero y obtiene s�lo su componente del path.
	 * 
	 * @param filename
	 *            The filename
	 * @return The path information

	 */
	public static String pathComponent(String filename) {
		int i = filename.lastIndexOf(File.separator);
		return (i > -1) ? filename.substring(0, i) : filename;
	}

	/**
	 * 
	 * Cerrar un fichero y tratar una posible excepci�n(poco probable).
	 * Ideal colocar al final del bloque de c�digo para asegurarse de la liberaci�n de recursos.
	 * 
	 * 
	 * @param c
	 *            Closeable resource para cerrar
	 * @return informaci�n del path

	 */
	public static void close(Closeable c) {
		if (c != null) {
			try {
				c.close();
			}
			catch (IOException e) {
				// ignorar o log
			}
		}
	}
}
