package com.navegador.gestortareas.tareas.complejas.oVirtual;

import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

import edu.dhbw.andar.ARObject;
import edu.dhbw.andar.pub.SimpleBox;
import edu.dhbw.andar.util.GraphicsUtil;

/**
 *Esta clase define las caracteristicas del objeto virtual a renderizar. 
 * Esta clase define las caracteristicas para dibujar un cubo a modo de ejemplo.
 * 
 *
 */
public class CustomObject extends ARObject {

	/**
	 * 
	 * @param name
	 * @param patternName
	 * @param markerWidth
	 * @param markerCenter
	 */
	public CustomObject(String name, String patternName,
			double markerWidth, double[] markerCenter) {
		super(name, patternName, markerWidth, markerCenter);
		float   mat_ambientf[]     = {0f, 1.0f, 0f, 1.0f};
		float   mat_flashf[]       = {0f, 1.0f, 0f, 1.0f};
		float   mat_diffusef[]       = {0f, 1.0f, 0f, 1.0f};
		float   mat_flash_shinyf[] = {50.0f};

		mat_ambient = GraphicsUtil.makeFloatBuffer(mat_ambientf);
		mat_flash = GraphicsUtil.makeFloatBuffer(mat_flashf);
		mat_flash_shiny = GraphicsUtil.makeFloatBuffer(mat_flash_shinyf);
		mat_diffuse = GraphicsUtil.makeFloatBuffer(mat_diffusef);
		
		
	}
	
	/**
	 * 
	 * @param name
	 * @param patternName
	 * @param markerWidth
	 * @param markerCenter
	 * @param customColor
	 */
	public CustomObject(String name, String patternName,
			double markerWidth, double[] markerCenter, float[] customColor) {
		super(name, patternName, markerWidth, markerCenter);
		float   mat_flash_shinyf[] = {50.0f};

		mat_ambient = GraphicsUtil.makeFloatBuffer(customColor);
		mat_flash = GraphicsUtil.makeFloatBuffer(customColor);
		mat_flash_shiny = GraphicsUtil.makeFloatBuffer(mat_flash_shinyf);
		mat_diffuse = GraphicsUtil.makeFloatBuffer(customColor);
		
	}
	
	/**
	 * Objeto virtual que se dibuja es un caja, esta clase se importa del proyecto AndAR
	 * 
	 */
	private SimpleBox box = new SimpleBox();
	private FloatBuffer mat_flash;
	private FloatBuffer mat_ambient;
	private FloatBuffer mat_flash_shiny;
	private FloatBuffer mat_diffuse;
		
	/**
	 * 
	 * En este m�todo se dibuja la figura con la libreria Open GL es
	 */
	@Override
	public final void draw(GL10 gl) {
		super.draw(gl);
		
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_SPECULAR,mat_flash);
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_SHININESS, mat_flash_shiny);	
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_DIFFUSE, mat_diffuse);	
		gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_AMBIENT, mat_ambient);

	    //dibujar cubo
	    // gl.glColor4f(0, 1.0f, 0, 1.0f);
	    gl.glColor4x(1, 0, 0, 0);
	    gl.glTranslatef( 0.0f, 0.0f, 12.5f );
	    //dibujar caja
	    box.draw(gl);
	}
	@Override
	public void init(GL10 gl) {
		
	}
}
