package com.navegador.gestortareas.tareas.complejas.qr;

import com.navegador.R;
import com.navegador.gestortareas.lanzador.IdParametros;
import com.navegador.gestortareas.tareas.ActividadTarea;

import jim.h.common.android.zxinglib.integrator.IntentIntegrator;
import jim.h.common.android.zxinglib.integrator.IntentResult;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;


/**
 * Actividad b�sica que realiza la tarea de obtener un c�digo QR con la visualizaci�n de la c�mara.
 * Esta clase hereda de ActividadTarea, se encarga de configurar la respuesta y enviarla. 
 * A trav�s de un Intent recibe dos par�metros; DSK y reciever.
 * 
 * Librerias necesarias: core.jar,zxingjar-1.1.jar
 * 
 * @author Guillermo
 *
 */
public class ActividadQR extends ActividadTarea{

	private Handler  handler = new Handler();
   
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		// Obtener datos del Intent
		Bundle bundle = getIntent().getExtras();
		this.DSK = bundle.getString(IdParametros.DSK);
		this.reciever = bundle.getString(IdParametros.reciever);
        // Lanzar actividad que escanea el c�digo
		IntentIntegrator.initiateScan(ActividadQR.this, R.layout.capture,
                R.id.viewfinder_view, R.id.preview_view, true);
    }

    /**
     * Este m�todo interpreta el resultado de la actividad lanzada por la misma. 
     * Dependiendo del resultado de la actividad lanzada se configura el objeto Respuesta.
     * 
     */
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case IntentIntegrator.REQUEST_CODE:
                IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode,
                        resultCode, data);
                if (scanResult == null) {
                    return;
                }
                final String result = scanResult.getContents();
                if (result != null) {
                    handler.post(new Runnable() {
                        public void run() {
                            enviarRespuesta(result);
                            finish();
                        }
                    });
                }
                break;
            default:
            	finish();
        }
    }

}
