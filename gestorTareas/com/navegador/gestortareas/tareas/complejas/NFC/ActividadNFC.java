package com.navegador.gestortareas.tareas.complejas.NFC;

import android.app.PendingIntent;
import android.content.Intent;
import android.nfc.FormatException;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import com.navegador.R;
import com.navegador.gestortareas.lanzador.IdParametros;
import com.navegador.gestortareas.tareas.ActividadTarea;

/**
 * Esta Actividad realiza las tareas propias la tarea NFC. Son dos tareas principales; leer un chip NFC y/o escribir sobre un chip NFC. 
 * La escritura en los Tags NFC se realiza en UTF-8 y con la localidad espa�ola.
 * 
 * A parte de realizar operaciones en el chip NFC, inicializa la Respuesta,
 *  la Respuesta envia los datos al servidor del tag leido y/o modificado
 *  
 * @author Guillermo
 *
 */
public class ActividadNFC extends ActividadTarea{
	
	private static final Locale LOCALIDAD = new Locale("es");

	private NfcAdapter nfcAdaptador;
	private PendingIntent pendingIntent;
	
	private EditText textoNFC;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nfc);
		// Obtener datos del Intent
		Bundle bundle = getIntent().getExtras();
		this.DSK = bundle.getString(IdParametros.DSK);
		this.reciever = bundle.getString(IdParametros.reciever);
		
		this.textoNFC = (EditText) this.findViewById(R.id.txtNFC);

		nfcAdaptador = NfcAdapter.getDefaultAdapter(this);
		pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
				getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

	}

	public void onResume() {
		super.onResume();
		
		// Comprobar la disponibilidad de NFC
		
		if (nfcAdaptador != null) {
			if (nfcAdaptador.isEnabled()) {
				//Se registra la posibilidad de leer NFC
				nfcAdaptador.enableForegroundDispatch(this, pendingIntent, null,
						null);
				runOnUiThread(new Runnable() {
					public void run() {
						Toast.makeText(ActividadNFC.this,
								"Por favor, aproximese a un chip NFC", Toast.LENGTH_LONG)
								.show();
					}
				});;
			} else {
				// Activar NFC en el dispositivo
				runOnUiThread(new Runnable() {
					public void run() {
						Toast.makeText(ActividadNFC.this,
								"Por favor, activa NFC", Toast.LENGTH_LONG)
								.show();
					}
				});
			}
		} else {
			runOnUiThread(new Runnable() {
				public void run() {
					Toast.makeText(ActividadNFC.this, "Dispositivo sin NFC",
							Toast.LENGTH_LONG).show();
				}
			});
			finish();
		}
	}

	public void onPause() {
		super.onPause();
		if (nfcAdaptador != null) {
			nfcAdaptador.disableForegroundDispatch(this);
		}
	}

	public void onNewIntent(Intent intent) {
		//Se crean diferentes hilos para ejecutar distintos procesos nfc
		new NfcProcesoHilo(intent).start();
	}

	public class NfcProcesoHilo extends Thread {
		private Intent intento;

		public NfcProcesoHilo(Intent intento) {
			this.intento = intento;
		}

		public void run() {
			Looper.prepare();
			final String action = intento.getAction();
			Log.d("Actividad NFC", "Action: " + action);
			final ClienteNFC nfcIntento = new ClienteNFC(intento);
			try {
				nfcIntento.leer(); // Se intenta leer el contenido del NFC tag
				final List<String> textoLeido = nfcIntento.getContenido();
				if (textoLeido != null) {
					final StringBuilder builder = new StringBuilder();
					for (final String texto : textoLeido) {
						if (builder.length() > 0) {
							builder.append(", ");
						}
						builder.append(texto);
					}
					runOnUiThread(new Runnable() {
						public void run() {
							Toast.makeText(getApplicationContext(),
									"Tag NFC leido", Toast.LENGTH_LONG)
									.show(); 
						}
					});
					enviarRespuesta(builder.toString());
					finish();
					
				}
			} catch (FormatException e) {
				//Error de formato de la etiqueta NFC

			} catch (IOException e) {
				//Error de entrada y salida
			}
			
			//Si el usuario ha introducido algo de texto en el EditText textoNFC
			final String texto = textoNFC.getText().toString().trim();
			if (texto.length() > 0) {
				try{
					nfcIntento.escribir(texto,LOCALIDAD,true);
					
					 runOnUiThread(
	                            new Runnable()
	                            {
	                                public void run() {
	                                    try
	                                    {
	                                    	Toast.makeText(getApplicationContext(),
	            									"Tag NFC escrito", Toast.LENGTH_LONG)
	            									.show(); 
	                                    }
	                                    catch( Exception ignored ) {}
	                                }
	                            }
	                    );
					
				}
				catch( Exception e )
                {
                    Log.e( "Actividad NFC", "Error al escribir tag", e );
                }
				
				
			}
			

		}

	}

}
