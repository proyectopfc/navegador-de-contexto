package com.navegador.gestortareas.tareas.complejas.NFC;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import android.content.Intent;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.util.Log;

/**
 * Esta clase implementa las operaciones b�sicas para trabajar con un chip NFC, operaciones de lectura, escritura,
 *  reconocimiento de meta-informaci�n.
 *  Esta clase es utilizada en la tarea ActividadNFC.
 * 
 * @author Guillermo
 *
 */
public class ClienteNFC {

	private Tag tagIntento;
	private boolean ndef;
	private boolean ndefFormatable;
	private List<String> contenido;

	
	/**
	 * Constructor, se crea un objeto ClienteNFC con el Intent que se pasa c�mo par�metro.
	 * 
	 * @param intento
	 */
	public ClienteNFC(Intent intento) {
		tagIntento = intento.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		for (String tech : tagIntento.getTechList()) {
			if (tech.equals(Ndef.class.getName())) {
				Log.d("ClienteNFC", "NFC con tecnolog�a Ndef");
				ndef = true;
			} else if (tech.equals(NdefFormatable.class.getName())) {
				Log.d("ClienteNFC", "NFC con tecnolog�a NdefFormatable");
				ndefFormatable = true;
			}
		}
	}
	
	/**
	 * M�todo que trata de leer el texto plano del Tag NFC con tecnolog�a Ndef.
	 * 
	 * Una vez leido, para obtener la lista de String del Tag NFC se utiliza el m�todo getRead().
	 * 
	 * @throws FormatException
	 * @throws IOException
	 */
	public void leer() throws FormatException, IOException {
		if (ndef) {
			leerNdefTag();
		}
	}
	
	/**
	 * M�todo que devuelve una lista de String con el texto del Tag NFC.
	 * 
	 * @return
	 */
	public List<String> getContenido() {
		return contenido;
	}
	
	/**
	 * M�todo interno que interpreta un tag NFC con tecnolog�a Ndef y lee su contenido.
	 * 
	 * @throws IOException
	 * @throws FormatException
	 */
	private void leerNdefTag() throws IOException, FormatException {
		final Ndef ndef = Ndef.get(tagIntento);
		try {
			ndef.connect();
			NdefMessage msg = ndef.getNdefMessage();
			NdefRecord[] records = msg.getRecords();
			if (records != null) {
				contenido = new Vector<String>(records.length);
				for (final NdefRecord record : records) {
					String texto = getTexto(record.getPayload());
					if (texto != null) {
						contenido.add(texto);
					}
				}
			} else {
				Log.d("ClienteNFC", "Tag vacio");
			}
		} finally {
			try {
				ndef.close();
			} catch (Exception ignored) {
			}
		}
	}

	/**
	 * El primer byte del vector payload contiene el estado de codificaci�n.
	 * 	bit 0-5: c�digo de lenguaje IANA.
	 * 	bit 6: est� a 0, se reserva su uso para el futuro 
	 * 	bit 7: tipo de codificaci�n, si el bit est� a 1 la codificaci�n es UTF16. Si el bit est� a 0 es UTF8.
	 * 
	 * @param payload
	 * @return
	 */
	private String getTexto(final byte[] payload) {
		if (payload != null) {
			try {
				String textEncoding = ((payload[0] & 0x80) == 0) ? "UTF-8"
						: "UTF-16";
				int languageCodeLength = payload[0] & 0x3f;
				return new String(payload, languageCodeLength + 1,
						payload.length - languageCodeLength - 1, textEncoding);
			} catch (Exception e) {
				Log.e("ClienteNFC", "Error payload", e);
			}
		}
		return null;
	}
	
	/**
	 * 
	 * Este m�todo p�blico se encarga de escribir en los registro de un chip NFC una determina cadena de texto que se pasa 
	 * como par�metro. Tambi�n se pasa como par�metro un objeto Locale que especifica el lenguaje del texto
	 *  y se indica en un booleano si el texto esta codificado en UTF-8. Si no esta codificado en UTF-8, se codifica en UTF-16.
	 *  Este m�todo permite escribir en los chips NFC que son virgenes, nunca se ha escrito nada en ellos, 
	 *  y modificar chips previamente escritos.
	 * 
	 * @param texto
	 * @param localidad
	 * @param esUTF8
	 * @throws IOException 
	 * @throws FormatException 
	 */
	public void escribir (final String texto, final Locale localidad, boolean esUTF8) throws IOException, FormatException{
		NdefRecord[] registros = { siguienteRegistro(texto, localidad, esUTF8) };
		NdefMessage message = new NdefMessage(registros);

		if (ndef){ //El tag NFC ya esta escrito
			Ndef ndef = Ndef.get(tagIntento);
			try {
				ndef.connect();
				ndef.writeNdefMessage(message);
			} finally {
				try {
					ndef.close();
				} catch (Exception ignored) {
				}
			}
		}
		else if (ndefFormatable){ // El tag NFC no esta escrito pero la tecnolog�a soporta escribir sobre el chip
			NdefFormatable ndefFormatable = NdefFormatable.get(tagIntento);
			try {
				ndefFormatable.connect();
				ndefFormatable.format(message);
			} finally {
				try {
					ndefFormatable.close();
				} catch (Exception ignored) {
				}
			}
		}
	}
	
	/**
	 * M�todo interno que escribe en los registros del chip un determinado texto, con su localidad
	 *  y tipo de codificaci�n.
	 * 
	 * @param texto
	 * @param localidad
	 * @param esUTF8
	 * @return registroNDF
	 */
	private NdefRecord siguienteRegistro(String texto, Locale localidad, boolean esUTF8 ){
		   byte[] langBytes = localidad.getLanguage().getBytes( Charset.forName( "US-ASCII" ));

	        Charset utfEncoding = esUTF8 ? Charset.forName("UTF-8") : Charset.forName("UTF-16");
	        byte[] textBytes = texto.getBytes(utfEncoding);

	        int utfBit = esUTF8 ? 0 : (1 << 7);
	        char status = (char) (utfBit + langBytes.length);

	        byte[] data = new byte[1 + langBytes.length + textBytes.length];
	        data[0] = (byte) status;
	        System.arraycopy(langBytes, 0, data, 1, langBytes.length);
	        System.arraycopy(textBytes, 0, data, 1 + langBytes.length, textBytes.length);

	        return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], data);
	}

}
