package com.navegador.gestortareas.tareas;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
/**
 * 
 * Clase que inicializa y obtiene el estado del sensor de luminosidad
 * 
 * @author Guillermo
 *
 */
public class SensorLuminosidad extends MiSensor{
	
	private Sensor luminosidad;

	public SensorLuminosidad(SensorManager sensorManager) {
		super(sensorManager);
		//Inicializa el sensor especifico de luminosidad
		this.luminosidad = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT); 
		//Se registra el sensor
		this.registrarSensor(this.luminosidad); 
	}

	/**
	 * Método que registra las mediciones del sensor de luminosidad. 
	 * En la unidad internacional para la iluminación, Lux.
	 */
	public void onSensorChanged(SensorEvent arg0) {
		float[] masData;
		switch (arg0.sensor.getType()) {
		case Sensor.TYPE_LIGHT:
			masData = arg0.values;
			x = masData[0];
			break;
		}
	}
}
