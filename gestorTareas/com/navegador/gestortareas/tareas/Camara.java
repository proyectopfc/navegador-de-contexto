package com.navegador.gestortareas.tareas;

import java.io.File;

import com.navegador.gestortareas.lanzador.IdParametros;

import datos.Configuraciones;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

/**
 * Actividad b�sica que se encarga de realizar una fotograf�a con la c�mara est�ndar del dispositivo,
 *  y env�a la fotograf�a al servidor a trav�s del objeto Respuesta
 * 
 * @author Guillermo
 *
 */

public class Camara extends ActividadTarea{
	private static final int CAPTURA_IMAGEN_ACTIVIDAD_CODIGO_RESPUESTA = 100; // entero que identifica el resultado de la actividad de la c�mara
	public static final int MEDIA_IMAGEN = 1; // entero que identifica un file como una im�gen
	
	private Uri fileUri; // Uri del fichero
	private static String nFile = Configuraciones.tempFile; //nombre del fichero
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Obtener datos del Intent
		Bundle bundle = getIntent().getExtras();
		this.DSK = bundle.getString(IdParametros.DSK);
		this.reciever = bundle.getString(IdParametros.reciever);
		nFile = DSK;
		//Lanzar actividad que realiza la fotografia
		this.fotografia();
	}
	
	/**
	 * M�todo que lanza la actividad para realizar una fotograf�a con la c�mara por defecto del dispositivo. 
	 * 
	 */
	private void fotografia(){
		// intento para capturar una fotografia
	    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	    // se crea el file y devuelve el path
	    fileUri = getOutputMediaFileUri(MEDIA_IMAGEN); 
	    // se a�ade al intento el path del file creado
	    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
	    // se inicia la actividad
	    startActivityForResult(intent, CAPTURA_IMAGEN_ACTIVIDAD_CODIGO_RESPUESTA);
	}
	
	/**
	 * M�todo que retorna el objeto Uri de un File.
	 * 
	 * @param type
	 * @return Uri
	 */
	private static Uri getOutputMediaFileUri(int type){
	      return Uri.fromFile(getOutputMediaFile(type));
	}
	
	/**
	 * Crea un objeto file para almacenar un fichero con extensi�n .jpg
	 *  Se almacena temporalmente en el directorio raiz de la memoria externa, en la carpeta definida en Configuraciones
	 *  y el nombre el valor de la variable nFile.
	 * @param type
	 * @return
	 */
	private static File getOutputMediaFile(int type){
		//assert Environment.getExternalStorageState() esta montado
	
	    File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), Configuraciones.tempCarpeta);
	     
	    if (! mediaStorageDir.exists()){
	        if (! mediaStorageDir.mkdirs()){
	            Log.d("Camara", "Fallo al crear el fichero");
	            return null;
	        }
	    }
	    
	    File mediaFile;
	    if (type == MEDIA_IMAGEN){
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        nFile + ".jpg");
	    }  else {
	        return null;
	    }
	    return mediaFile;
	}
	
	/**
	 * M�todo que interpreta el resultado de la actividad lanzada por la misma.
	 * Dependiendo de la resultado se configura el objeto Respuesta.
	 * 
	 */
	public void onActivityResult (int requestCode, int resultCode, Intent data){
		// respuesta de la actividad de que se encarga de realizar una
		// fotograf�a.
		if (requestCode == CAPTURA_IMAGEN_ACTIVIDAD_CODIGO_RESPUESTA) {
			if (resultCode == RESULT_OK) {
				// Fotografia realizada
				Toast.makeText(
						this,
						"Fotografia guardada temporalmente en :\n"
								+ fileUri.getEncodedPath(), Toast.LENGTH_LONG)
						.show();
				// se envia el fichero al servidor; se pasa como par�metro el path temporal del file
				enviarFile(fileUri.getEncodedPath(), DSK);
				// se finaliza la actividad
				finish();
			} else if (resultCode == RESULT_CANCELED) {
				finish();
			} else {
				Toast.makeText(this, "Fotografia no guardada",
						Toast.LENGTH_LONG).show();
				finish();
			}
		}
	}
}
