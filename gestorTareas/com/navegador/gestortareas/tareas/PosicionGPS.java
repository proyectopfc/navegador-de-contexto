package com.navegador.gestortareas.tareas;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

/**
 * Clase que obtiene la posici�n GPS del dispositivo android.
 * 
 * @author Guillermo
 *
 */

public class PosicionGPS extends DatosContexto implements LocationListener{

	private LocationManager lManager;
	
	/**
	 * Constructor
	 * 
	 * El par�metro lManager se obtiene con el m�todo (LocationManager)getSystemService(Context.LOCATION_SERVICE) 
	 * en la actividad que inicializa el objeto PosicionGPS de la aplicaci�n.
	 * 
	 * @param lManager
	 */
	public PosicionGPS(LocationManager lManager){
		this.lManager = lManager;
		
		//lmanager.getLastKnownLocation(LocationManager.GPS_PROVIDER); 
		//se registra la localizaci�n v�a GPS con una frecuencia de 30000 milisegundos
		this.lManager.requestLocationUpdates(
    			LocationManager.GPS_PROVIDER, 30000, 0, this);
	}

	/**
	 * M�todo que registra las variables de la localizaci�n cada vez existe un cambio
	 */
	public void onLocationChanged(Location location) {
		this.x = location.getLatitude();
		this.y = location.getLongitude();
		this.z = location.getAltitude();
		
	}

	public void onProviderDisabled(String provider) {
		
		
	}

	public void onProviderEnabled(String provider) {
		
		
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		
		
	}
}
