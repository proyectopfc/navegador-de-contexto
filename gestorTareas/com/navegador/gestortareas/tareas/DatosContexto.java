package com.navegador.gestortareas.tareas;
/**
 * Clase que define los atributos y los m�todos mas importantes 
 * para obtener los datos de contexto.
 * 
 * @author Guillermo
 *
 */
public class DatosContexto {
	
	public double x,y,z;// son los valores que representan al sensor en cada instante 
	private double oldX,oldY,oldZ;// �ltimos valores registrados

	private boolean esPrimeraMedicion;
	
	/**
	 * Contructor
	 */
	public DatosContexto(){
		this.esPrimeraMedicion = true;
	}
	
	public double getX(){
		return this.oldX;
	}
	public double getY(){
		return this.oldY;
	}
	public double getZ(){
		return this.oldZ;
	}

	/**
	 * M�todo que registra las mediciones del sensor en funci�n de dos par�metros:
	 * -sensitive: condiciona a que se registre el valor si la �ltima medici�n obtenida sea diferente
	 * -accuacy: especifica un rango minimo para considerar que dos mediciones sean diferentes.
	 * 
	 * @param sensitive
	 * @param accuacy
	 */
	public void medicion(boolean sensitive, double accuacy){
		if(sensitive == false || this.esPrimeraMedicion){
			this.oldX = x;
			this.oldY = y;
			this.oldZ = z;
		}
		else{ //sensitive == true
			if(rangoMayor(accuacy,x,oldX)){
				this.oldX = x;
			}
			if(rangoMayor(accuacy,y,oldY)){
				this.oldY = y;
			}
			if(rangoMayor(accuacy,z,oldZ)){
				this.oldZ = z;
			}
		}
		this.esPrimeraMedicion = false;
	}
	
	/**
	 * M�todo interno que comprueba si la nueva medici�n es superior a la anterior.
	 * Retorna true si el rango es mayor.
	 * @param accuacy
	 * @return
	 */
	private boolean rangoMayor(double accuacy,double n, double oldN){
		if((oldN-n)>=accuacy || (n-oldN)>=accuacy){
			return true;
		}
		return false;
	}
	

	
}
