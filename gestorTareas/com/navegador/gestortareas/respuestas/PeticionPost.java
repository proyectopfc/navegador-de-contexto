package com.navegador.gestortareas.respuestas;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
/**
 * Esta clase implementa los m�todos para realizar una petici�n post, enviar propiedades,
 *  enviar fichero y obtener la respuesta de la petici�n
 * 
 * @author Guillermo
 *
 */
public class PeticionPost {
	private URL url;
	String propiedades;

	public PeticionPost(String url) throws MalformedURLException {
		this.url = new URL(url);
		propiedades = "";
	}
	/**
	 * Metodo que a�ade propiedades a la cadena data.
	 * 
	 * @param propiedad
	 * @param valor
	 * @throws UnsupportedEncodingException
	 */
	public void a�adir(String propiedad, String valor)
			throws UnsupportedEncodingException {
		// codificamos cada uno de los valores
		if (propiedades.length() > 0)
			propiedades += "&" + URLEncoder.encode(propiedad, "UTF-8") + "="
					+ URLEncoder.encode(valor, "UTF-8");
		else
			propiedades += URLEncoder.encode(propiedad, "UTF-8") + "="
					+ URLEncoder.encode(valor, "UTF-8");
	}
	/**
	 * Este m�todo realiza una petici�n post
	 * @throws IOException
	 */

	public void enviarPeticion() throws IOException {
		// abrimos la conexi�n
		URLConnection conn = url.openConnection();
		// especificamos que vamos a escribir
		conn.setDoOutput(true);
		// obtenemos el flujo de escritura
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		// escribimos
		wr.write(propiedades);
		// cerramos la conexi�n
		wr.close();

	}
	/**
	 * Este m�todo realiza una petici�n post y retorna la respuesta en un String
	 * 
	 * @return
	 * @throws IOException
	 */
	public String getRespueta() throws IOException {
		String respuesta = "";
		// se abre la conexi�n
		URLConnection conn = url.openConnection();
		// se especifica que se va a escribir
		conn.setDoOutput(true); 
		// se obtiene el flujo de escritura
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		// se escriben las propiedades
		wr.write(propiedades);
		// se cierra la conexi�n
		wr.close();
		// se obtiene el flujo de lectura
		BufferedReader rd = new BufferedReader(new InputStreamReader(
				conn.getInputStream()));
		String linea;
		// se procesa la respuesta
		while ((linea = rd.readLine()) != null) {
			respuesta += linea;
		}
		
		return respuesta;
	}
	/**
	 * Este m�todo envia un archivo a trav�s de una petici�n post
	 * y retorna la respuesta http de la petici�n.
	 * 
	 * Notas:Tener en cuenta los permisos de carpeta del servidor
	 * 
	 * @param pathFile Path local del fichero que se envia
	 * @return
	 */
	public String enviarFile(String pathFile,String DSK){
		HttpURLConnection connection = null;
		DataOutputStream outputStream = null;
		DataInputStream inputStream = null;

		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary =  "*****";

		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1*1024*1024;

		try
		{
		FileInputStream fileInputStream = new FileInputStream(new File(pathFile) );

		connection = (HttpURLConnection) url.openConnection();

		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setUseCaches(true);

		//Se habilita el m�todo POST
		connection.setRequestMethod("POST");
		
		connection.setRequestProperty("Connection", "Keep-Alive");
		connection.addRequestProperty("DSK", DSK);
		connection.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary);
		
		
		
		outputStream = new DataOutputStream( connection.getOutputStream() );
		//outputStream.writeBytes(data);//Se escribe los datos
		outputStream.writeBytes(twoHyphens + boundary + lineEnd);
		outputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\"" + pathFile +"\"" + lineEnd);
		outputStream.writeBytes(lineEnd);

		bytesAvailable = fileInputStream.available();
		bufferSize = Math.min(bytesAvailable, maxBufferSize);
		buffer = new byte[bufferSize];

		//Se lee el fichero
		bytesRead = fileInputStream.read(buffer, 0, bufferSize);
		while (bytesRead > 0){
			outputStream.write(buffer, 0, bufferSize);
			bytesAvailable = fileInputStream.available();
			bufferSize = Math.min(bytesAvailable, maxBufferSize);
			bytesRead = fileInputStream.read(buffer, 0, bufferSize);
		}

		outputStream.writeBytes(lineEnd);
		outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

		//Se obtiene la respuesta de la conexi�n
		String respuesta = connection.getResponseMessage();
		fileInputStream.close();
		outputStream.flush();
		outputStream.close();
		
		
		return respuesta;
		}
		catch (Exception ex)
		{	
			System.out.println(ex.getMessage());
		}
		return "Error";
		
		
	}
	
	

}