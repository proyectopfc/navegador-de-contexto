package com.navegador.gestortareas.respuestas;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

/**
 * Clase Respuesta, esta clase envia los datos obtenidos de las etiquetas al servidor.
 *  Inicializa una petici�n de tipo post para enviar los datos necesarios.
 * 
 * @author Guillermo
 *
 */
public class Respuesta {

	private PeticionPost peticion;
	
	private String reciever;
	 
	
	/**
	 * Constructor que inicializa la PeticionPost con el par�metro reciever
	 * @param reciever
	 */
	public Respuesta(String reciever) {
		this.reciever = reciever;

		try {
			
			peticion = new PeticionPost(this.reciever);		
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
	
			e.printStackTrace();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
		
			e.printStackTrace();
		
		}
		
	
	}

	/**
	 * Metodo que a�ade a la petici�n un par�metro
	 * @param id
	 * @param value
	 */
	public void a�adir(String id, String value){
		try {
			peticion.a�adir(id, value);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * M�todo que realiza la petici�n
	 * 
	 */
	public void peticion(){
		try {
			peticion.enviarPeticion();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getPeticion(){
		try {
			return peticion.getRespueta();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "Error";
	}
	/**
	 * 
	 * 
	 * @param path
	 * @param DSK
	 */
	public void enviarFile(String path,String DSK){
		peticion.enviarFile(path,DSK);
	}
	
}
