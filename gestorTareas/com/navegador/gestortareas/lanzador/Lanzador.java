package com.navegador.gestortareas.lanzador;


import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.navegador.gestorEtiquetas.Visualizador.Etiqueta;
import com.navegador.gestortareas.respuestas.Respuesta;
import com.navegador.gestortareas.tareas.DatosContexto;

/**
 * Esta clase representa el proceso de lanzar una tarea e instancia al objeto respuesta.
 * 
 * 
 * @author Guillermo
 *
 */
public class Lanzador{
	
	private DatosContexto datos;//Clase abstracta que representa a todas la posibles tareas
	private Etiqueta etiqueta;//Referencia de la etiqueta que instanci� el Lanzador
	
	private Respuesta respuesta;// M�dulo de respuesta
	
	private Handler mHandler;
	private int timeInterMilli;
	

	/**
	 * Constructor que lanza la medici�n de una tarea, si se ha definido un tiempo de intervalo de medici�n del sensor
	 *  se introducen Runnables en la cola de mensajes del navegador. 
	 * @param datos
	 * @param sensitive
	 * @param accuacy
	 */
	public Lanzador(DatosContexto datos,Etiqueta etiqueta,boolean sensitive,double accuacy) {
		this.datos = datos;
		this.etiqueta = etiqueta;
		this.mHandler = new Handler();
		timeInterMilli = etiqueta.getTimeInterval() * 1000;
		if(etiqueta.getTimeInterval()!=0){
			Runnable r = new Runnable(){
				public void run(){
					rutina();
					mHandler.postDelayed(this,timeInterMilli);
					//mHandler.removeCallbacks(this); PARAR
				}
			};
			mHandler.postDelayed(r,timeInterMilli);
			
		}else{
			this.rutina();
		}
	}
	
	/**
	 * Constructor que lanza la medici�n de una tarea; los par�metros sensitive y accuacy,
	 *  son false y 0 respectivamente.
	 * @param datos
	 */
	public Lanzador(DatosContexto datos,Etiqueta etiqueta){
		this.datos = datos;
		this.etiqueta = etiqueta;
		boolean sensitive = false;
		double accuacy = 0;
		//Se toma la medici�n 
		datos.medicion(sensitive, accuacy);	
		//Iniciliza la respuesta de datos; consiste en una petici�n http
		respuesta = new Respuesta(etiqueta.getReciever());
		//Se a�aden los datos a enviar
		envioDatos();
		//Se realiza la petici�n
		//respuesta.peticion();
		Log.i("Respuesta",respuesta.getPeticion()); 
		//Borramos de la vista el bot�n que hace referencia a la etiqueta, dejarlo visible o invisible
		etiqueta.botonInvisible();
	}
	
	/**
	 * Constructor que lanza las tareas que heredan de Activity.
	 * Se inicia la actividad y se envian los par�metros DSK y reciever a la actividad.
	 * @param etiqueta
	 * @param intent
	 */
	public Lanzador(Intent intent,Etiqueta etiqueta){
		intent.putExtra(IdParametros.DSK, etiqueta.getDSK());
		intent.putExtra(IdParametros.reciever, etiqueta.getReciever());
		intent.putExtra(IdParametros.marca,etiqueta.getMarca());
		intent.putExtra(IdParametros.url,etiqueta.getColor());
		etiqueta.getNavegador().startActivity(intent);
		etiqueta.botonInvisible();
	}

	/**
	 * Este m�todo implementa la rutina de la clase Lanzador
	 */
	private void rutina(){
		// Se toma la medici�n
		datos.medicion(etiqueta.getSensitive(), etiqueta.getAccuacy());
		// Inicializa la respuesta
		respuesta = new Respuesta(etiqueta.getReciever());
		// Se a�aden los datos a enviar
		envioDatos();
		// Se realiza la petici�n
		Log.i("Respuesta", respuesta.getPeticion());//respuesta.peticion();
		// Borramos de la vista el bot�n que hace referencia a la etiqueta,
		// dejarlo visible o invisible
		etiqueta.botonInvisible();
	}
	
	/**
	 * M�todo interno que a�ade a la respuesta los par�metros;DSK,x,y,z
	 */
	private void envioDatos(){
		respuesta.a�adir(IdParametros.DSK,etiqueta.getDSK());
		respuesta.a�adir(IdParametros.x,String.valueOf(datos.getX()) );
		respuesta.a�adir(IdParametros.y,String.valueOf(datos.getY()) );
		respuesta.a�adir(IdParametros.z,String.valueOf(datos.getZ()) );
		
	}
	
}
