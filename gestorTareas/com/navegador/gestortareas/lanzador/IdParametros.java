package com.navegador.gestortareas.lanzador;
/**
 * Clase estática con los identificadores de los parámetros
 * @author Guillermo
 *
 */
public class IdParametros {

	public static final String DSK = "DSK";
	public static final String x = "x";
	public static final String y = "y";
	public static final String z = "z";
	public static final String cadena = "cadena";
	public static final String reciever = "reciever";
	public static final String marca = "marca"; //Objeto Virtual
	public static final String url = "url"; // URL objeto virtual

}
