package com.navegador.gestorEtiquetas;

import com.navegador.gestorEtiquetas.AnalizadorXML.AnalizadorXML;
import com.navegador.gestorEtiquetas.Visualizador.Visualizador;
import com.navegador.navegadorEstandar.Navegador;

import android.webkit.WebView;
import android.content.pm.PackageManager;

/**
 * M�dulo Gestor de etiquetas, este clase se encarga de inicializar el analizador de xml y el visualizado de etiquetas xml
 * 
 * @author Guillermo
 *
 */

public class GestorEtiquetas {
	
	private PackageManager pManager;
	private String url;
	private WebView webView;
	
	private AnalizadorXML analizadorXML;
	private Visualizador visualizador; 

	/**
	 * Constructor, se encarga de inicializar las variables de la clase.
	 * 
	 * @param nav
	 */
	public GestorEtiquetas(Navegador nav){
		this.pManager = nav.getPackageManager();
		this.url = nav.getUrl();
		this.webView = nav.getWebView();
		//ANALIZADOR XML: Analiza etiquetas del c�digo de la url, analiza etiquetas disponibles en el dispositivo, crea una lista final
		analizadorXML = new AnalizadorXML(url,pManager);
		//VISUALIZADOR
		visualizador = new Visualizador(analizadorXML.getTags(),nav);//Par�metros:ArrayList con los tags para el visualizador, la actividad navegador
	}

}
