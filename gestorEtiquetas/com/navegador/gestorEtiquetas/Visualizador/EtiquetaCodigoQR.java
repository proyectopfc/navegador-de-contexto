package com.navegador.gestorEtiquetas.Visualizador;

import org.w3c.dom.Node;

import android.content.Intent;
import android.view.View;

import com.navegador.gestortareas.lanzador.Lanzador;
import com.navegador.gestortareas.tareas.complejas.qr.ActividadQR;
import com.navegador.navegadorEstandar.Navegador;

public class EtiquetaCodigoQR extends Etiqueta {
	
	private Intent intento;
	
	public EtiquetaCodigoQR(Navegador nav, Node nodo) {
		super(nav, nodo);
		intento = new Intent(etiqueta.getNavegador(),ActividadQR.class);
		escucharEventos();
	}

	/**
	 * M�todo que escucha el evento del bot�n de la etiqueta e inicializa el lanzador
	 */
	public void escucharEventos() {
		this.boton.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				//se abre una canal de comunicaci�n con la capa de gestorTareas-Lanzador de tareas
				lanzador = new Lanzador(intento,etiqueta);
			}
        });

	}

}
