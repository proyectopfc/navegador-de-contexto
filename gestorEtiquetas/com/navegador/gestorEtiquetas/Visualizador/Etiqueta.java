package com.navegador.gestorEtiquetas.Visualizador;

import java.util.ArrayList;

import org.w3c.dom.Node;

import com.navegador.gestorEtiquetas.AnalizadorXML.AnalizadorXML;
import com.navegador.gestorEtiquetas.AnalizadorXML.TagsEtiquetas;
import com.navegador.gestortareas.lanzador.Lanzador;
import com.navegador.gestortareas.tareas.DatosContexto;
import com.navegador.navegadorEstandar.Navegador;

import android.view.View;
import android.webkit.WebView;
import android.widget.AbsoluteLayout;
import android.widget.Button;

/**
 * Clase abstracta que define los aspectos generales de todas las etiquetas que se visualizan en webView.
 * Cada etiqueta implementa su m�todo propio de escucha de eventos y reacciona de distinta manera.
 * 
 * @author Guillermo
 *
 */
public abstract class Etiqueta {
	
	protected Navegador nav;
	protected Button boton;
	private WebView webView;
	
	protected DatosContexto datos;//Objeto abstracto que interact�a con la API para obtener los datos de contexto
	protected Lanzador lanzador;//Capa del sistema
	protected Etiqueta etiqueta;
	
	private AnalizadorXML analizadorXML;//Este objeto permite analizar las propiedades de cada Nodo.

	//configuraciones EN XML
	private String id;// Identificador �nico para cada etiqueta
	private int timeInterval;
	private String reciever;// web que recibe los datos y los muestra
	private String text;//Cadena de texto que se incluye en el bot�n
	private int x,y; //posici�n 
	private int ancho,alto; //tama�o del bot�n que representa a la etiqueta
	protected boolean sensitive;
	protected double accuacy;
	private String tipo; // gps o net
	private String marca; // marca para la representaci�n del objeto virtual: "patt.hiro" , "android.patt", "barcode.patt"
	private String url;
	
	/**
	 * Constructor
	 * @param nav: Navegador, representa a la vista donde se representa la etiqueta
	 * @param nodo: Node, encapsula la informaci�n obtenida del XML
	 */
	public Etiqueta(Navegador nav, Node nodo){
		this.nav = nav;
		//Obtiene el webView del navegador
		this.webView = nav.getWebView();
		//Inicializa el bot�n 
		boton = new Button(nav);
		//Configurar las propiedades de la etiqueta
		this.configuracion(nodo);
		//Representa el bot�n en el webView del navegador
		webView.addView(boton, new AbsoluteLayout.LayoutParams(ancho, alto, x, y));
		//guardar la referencia del objeto
		this.etiqueta = this;
		
		this.setTexto(text);
		
		this.botonVisible();
		
	}
	
	public abstract void escucharEventos();
	
	public String getReciever(){
		return this.reciever;
	}
	
	public int getTimeInterval(){
		return this.timeInterval;
	}
	
	public String getTipo(){
		return tipo;
	}
	
	public boolean getSensitive(){
		return this.sensitive;
	}
	
	public double getAccuacy(){
		return this.accuacy;
	}
	
	public String getMarca(){
		return this.marca;
	}
	
	public String getColor(){
		return this.url;
	}
	
	public void botonVisible(){
		boton.setVisibility(View.VISIBLE);
	}
	
	public void botonInvisible(){
		boton.setVisibility(View.INVISIBLE);
		
	}
	
	public void setTexto(String texto){
		boton.setText(texto);
	}
	
	public String getTexto(){
		return this.text;
	}
	
	public void setIcon(){
		//Icono, m�todo depreciado
		// buttonGetCompass.setBackgroundDrawable(getResources().getDrawable(R.drawable.compassicon));
	}
	
	public String getDSK(){
		return nav.getDSK();
	}
	
	public Navegador getNavegador(){
		return this.nav;
	}
	
	/**
	 * Analiza la configuraci�n de un nodo
	 * @param nodo
	 */
	private void configuracion(Node nodo){
		//Configuraci�n est�ndar
		this.configuracionEstandar();
		this.analizadorXML = new AnalizadorXML();
		ArrayList lista =
		this.analizadorXML.getPropiedades(nodo);
		
		for(int i = 0; i<lista.size();i++){
			propiedad((Node)lista.get(i));
		}
	}
	/**
	 * Obtiene el valor de cada Nodo propiedad y lo guarda en los registros correspondientes
	 * @param nodoP
	 */
	private void propiedad(Node nodoP){
		
		if(nodoP.getNodeName().equals(TagsEtiquetas.id)){
			this.id = nodoP.getNodeValue();
		}
		if(nodoP.getNodeName().equals(TagsEtiquetas.timeInterval)){
			this.timeInterval = Integer.valueOf(nodoP.getNodeValue());
		}
		if(nodoP.getNodeName().equals(TagsEtiquetas.reciever)){
			this.reciever = nodoP.getNodeValue();
		}
		if(nodoP.getNodeName().equals(TagsEtiquetas.text)){
			this.text = nodoP.getNodeValue();
		}
		if(nodoP.getNodeName().equals(TagsEtiquetas.x)){
			this.x = Integer.valueOf(nodoP.getNodeValue());
		}
		if(nodoP.getNodeName().equals(TagsEtiquetas.ancho)){
			this.ancho = Integer.valueOf(nodoP.getNodeValue());
		}
		if(nodoP.getNodeName().equals(TagsEtiquetas.alto)){
			this.alto = Integer.valueOf(nodoP.getNodeValue());
		}
		if(nodoP.getNodeName().equals(TagsEtiquetas.y)){
			this.y = Integer.valueOf(nodoP.getNodeValue());
		}
		if(nodoP.getNodeName().equals(TagsEtiquetas.sensitive)){
			this.sensitive = Boolean.parseBoolean(nodoP.getNodeValue());
		}
		if(nodoP.getNodeName().equals(TagsEtiquetas.accuacy)){
			this.accuacy = Double.valueOf(nodoP.getNodeValue());
		}
		if(nodoP.getNodeName().equals(TagsEtiquetas.tipo)){
			this.tipo = nodoP.getNodeValue();
		}
		if(nodoP.getNodeName().equals(TagsEtiquetas.marca)){
			this.marca = nodoP.getNodeValue();
		}
		if(nodoP.getNodeName().equals(TagsEtiquetas.url)){
			this.url = nodoP.getNodeValue();
		}
	}
	/**
	 * Configuraci�n por defecto de algunas propiedades de las etiquetas xml.
	 * En el caso de que la propiedad no este definida en la etiqueta xml, se definen por defecto
	 * 
	 */
	private void configuracionEstandar(){
		this.id = "noID";
		this.timeInterval = 0;
		this.reciever = "http://";
		this.text = "Sensor";
		this.x = 100;
		this.y = 100;
		this.ancho = 100;
		this.alto = 100;
		this.sensitive = false;
		this.accuacy = 0;
		this.tipo ="net";//gps o net (TagsEtiquetas)
		this.marca = "patt.hiro";
		this.url = "blanco";
		
	}
}
