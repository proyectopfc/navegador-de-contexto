package com.navegador.gestorEtiquetas.Visualizador;

import org.w3c.dom.Node;

import com.navegador.gestortareas.lanzador.Lanzador;
import com.navegador.gestortareas.tareas.SensorAcelerometro;
import com.navegador.navegadorEstandar.Navegador;

import android.hardware.SensorManager;
import android.view.View;

public class EtiquetaAcelerometro extends Etiqueta{
	
	public EtiquetaAcelerometro(Navegador nav, Node nodo){
		super(nav,nodo);
		//Inicializamos el sensor correspondiente
		this.datos = new SensorAcelerometro((SensorManager) nav.getSystemService(nav.SENSOR_SERVICE));
		//Escucha los eventos del bot�n e iniciliaza el lanzador de tareas
		escucharEventos();
	}
	
	/**
	 * M�todo que escucha el evento del bot�n de la etiqueta e inicializa el lanzador
	 */
	public void escucharEventos(){
		this.boton.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				//se abre una canal de comunicaci�n con la capa de gestorTareas-Lanzador de tareas
				lanzador = new Lanzador(datos,etiqueta,sensitive,accuacy);
			}
        });
	}
}
