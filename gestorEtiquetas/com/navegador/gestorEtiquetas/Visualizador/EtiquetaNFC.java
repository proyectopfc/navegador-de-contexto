package com.navegador.gestorEtiquetas.Visualizador;

import org.w3c.dom.Node;
import android.content.Intent;
import android.view.View;

import com.navegador.gestortareas.lanzador.Lanzador;
import com.navegador.gestortareas.tareas.complejas.NFC.ActividadNFC;
import com.navegador.navegadorEstandar.Navegador;

public class EtiquetaNFC extends Etiqueta {
	
	private Intent intento;

	public EtiquetaNFC(Navegador nav, Node nodo) {
		super(nav, nodo);
		intento = new Intent(etiqueta.getNavegador(),ActividadNFC.class);
		escucharEventos();
	}

	@Override
	public void escucharEventos() {
		this.boton.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				//se abre una canal de comunicación con la capa de gestorTareas-Lanzador de tareas
				lanzador = new Lanzador(intento,etiqueta);
			}
        });
	}

}
