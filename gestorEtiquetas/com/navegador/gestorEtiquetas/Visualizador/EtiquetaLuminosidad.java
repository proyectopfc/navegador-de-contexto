package com.navegador.gestorEtiquetas.Visualizador;

import org.w3c.dom.Node;

import com.navegador.gestortareas.lanzador.Lanzador;
import com.navegador.gestortareas.tareas.SensorLuminosidad;
import com.navegador.navegadorEstandar.Navegador;

import android.hardware.SensorManager;
import android.view.View;


public class EtiquetaLuminosidad extends Etiqueta {

	public EtiquetaLuminosidad(Navegador nav, Node nodo){
		super(nav,nodo);
		//Inicializamos el sensor correspondiente
		this.datos = new SensorLuminosidad((SensorManager) nav.getSystemService(nav.SENSOR_SERVICE));
		//Escucha los eventos del botón e iniciliaza el lanzador de tareas
		escucharEventos();
	}

	@Override
	public void escucharEventos() {
		this.boton.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				//se abre una canal de comunicación con la capa de gestorTareas-Lanzador de tareas
				lanzador = new Lanzador(datos,etiqueta,sensitive,accuacy);
			}
        });
	}

}
