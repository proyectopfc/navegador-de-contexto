package com.navegador.gestorEtiquetas.Visualizador;

import java.util.ArrayList;

import org.w3c.dom.Node;

import com.navegador.gestorEtiquetas.AnalizadorXML.TagsEtiquetas;
import com.navegador.navegadorEstandar.Navegador;

import android.webkit.WebView;

/**
 * M�dulo que se encarga de crear y visualizar las etiquetas xml obtenidas en el analizador xml.
 * 
 * @author Guillermo
 *
 */
public class Visualizador {
	
	private WebView webView;
	private Navegador nav; // Actividad navegador
	private ArrayList listaTags; // Lista con los tags a representar visualmente
	private ArrayList listaEtiquetas; // Lista con las etiquetas que han sido representadas visualmente

	/**
	 * Constructor; inicializa las variables de la clase y lanza la tarea visualizar.
	 * 
	 * @param lista
	 * @param nav
	 */
	public Visualizador(ArrayList lista,Navegador nav){
		this.webView = nav.getWebView();
		this.nav = nav;
		this.listaTags = lista;
		
		this.visualizar();
	}

	/**
	 * Recorre la lista de manera iterativa y crea los objeto para su representaci�n gr�fica
	 * y guarda la referencia en la lista listaEtiquetas
	 */
	private void visualizar(){
		
		listaEtiquetas = new ArrayList();
		
		Node nodo;
		
		for(int i = 0; i<listaTags.size(); i++){
		
			nodo = (Node)listaTags.get(i);
			
			listaEtiquetas.add(crearEtiquetas(idEtiquetas(nodo.getNodeName()),nodo));
		}
			
	}
	
	/**
	 * M�todo interno que crea e inicializa el objeto etiqueta, dependiendo del identificador 
	 * 
	 * @param id
	 * @param nodo
	 * @return
	 */
	private Etiqueta crearEtiquetas(int id, Node nodo){
		Etiqueta etiqueta = null;
		switch(id){

		case TagsEtiquetas.idAcelerometro:
			etiqueta = new EtiquetaAcelerometro(nav,nodo);
			return etiqueta;
		case TagsEtiquetas.idCamara:
			etiqueta = new EtiquetaCamara(nav,nodo);
			return etiqueta;
		case TagsEtiquetas.idoVirtual:
			etiqueta = new EtiquetaoVirtual(nav,nodo);
			return etiqueta;
		case TagsEtiquetas.idCodigoQR:
			etiqueta = new EtiquetaCodigoQR(nav,nodo);
			return etiqueta;
		case TagsEtiquetas.idNFC:
			etiqueta = new EtiquetaNFC(nav,nodo);
			return etiqueta;
		case TagsEtiquetas.idOrientacion:
			etiqueta = new EtiquetaOrientacion(nav,nodo);
			return etiqueta;
		case TagsEtiquetas.idPosicion:
			etiqueta = new EtiquetaPosicion(nav,nodo);
			return etiqueta;
		case TagsEtiquetas.idLuminosidad:
			etiqueta = new EtiquetaLuminosidad(nav,nodo);
			return etiqueta;
		case TagsEtiquetas.idProximidad:
			etiqueta = new EtiquetaProximidad(nav,nodo);
			return etiqueta;
		}
		return null;	
	}
	
	/**
	 * M�todo interno que retorna una identificaci�n num�rica por cada identificador de texto de una etiqueta.
	 * @param etiqueta
	 * @return
	 */
	private int idEtiquetas(String etiqueta){
		if(etiqueta.equals(TagsEtiquetas.acelerometro)) return TagsEtiquetas.idAcelerometro;
		if(etiqueta.equals(TagsEtiquetas.camara)) return TagsEtiquetas.idCamara;
		if(etiqueta.equals(TagsEtiquetas.codigoQR)) return TagsEtiquetas.idCodigoQR;
		if(etiqueta.equals(TagsEtiquetas.nfc)) return TagsEtiquetas.idNFC;
		if(etiqueta.equals(TagsEtiquetas.orientacion)) return TagsEtiquetas.idOrientacion;
		if(etiqueta.equals(TagsEtiquetas.oVirtual)) return TagsEtiquetas.idoVirtual;
		if(etiqueta.equals(TagsEtiquetas.posicion)) return TagsEtiquetas.idPosicion;
		if(etiqueta.equals(TagsEtiquetas.luminosidad)) return TagsEtiquetas.idLuminosidad;
		if(etiqueta.equals(TagsEtiquetas.proximidad)) return TagsEtiquetas.idProximidad;
		else{
			return 0;
		}
	}
}
