package com.navegador.gestorEtiquetas.Visualizador;

import org.w3c.dom.Node;

import android.hardware.SensorManager;
import android.view.View;

import com.navegador.gestortareas.lanzador.Lanzador;
import com.navegador.gestortareas.tareas.SensorProximidad;
import com.navegador.navegadorEstandar.Navegador;

public class EtiquetaProximidad extends Etiqueta {

	public EtiquetaProximidad(Navegador nav, Node nodo){
		super(nav,nodo);
		//Inicializamos el sensor correspondiente
		this.datos = new SensorProximidad((SensorManager) nav.getSystemService(nav.SENSOR_SERVICE));
		//Escucha los eventos del botón e iniciliaza el lanzador de tareas
		escucharEventos();
	}

	@Override
	public void escucharEventos() {
		this.boton.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				//se abre una canal de comunicación con la capa de gestorTareas-Lanzador de tareas
				lanzador = new Lanzador(datos,etiqueta,sensitive,accuacy);
			}
        });
	}

}
