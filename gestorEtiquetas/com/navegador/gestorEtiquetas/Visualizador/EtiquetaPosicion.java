package com.navegador.gestorEtiquetas.Visualizador;

import org.w3c.dom.Node;

import android.R;
import android.content.Context;
import android.location.LocationManager;
import android.view.View;

import com.navegador.gestorEtiquetas.AnalizadorXML.TagsEtiquetas;
import com.navegador.gestortareas.lanzador.Lanzador;
import com.navegador.gestortareas.tareas.PosicionGPS;
import com.navegador.gestortareas.tareas.PosicionNetwork;
import com.navegador.navegadorEstandar.Navegador;

public class EtiquetaPosicion extends Etiqueta {

	public EtiquetaPosicion(Navegador nav, Node nodo) {
		super(nav, nodo);
		
		if(this.getTexto().equalsIgnoreCase("Sensor")){
			boton.setText("");
			boton.setBackgroundDrawable(this.getNavegador().getResources().getDrawable(R.drawable.ic_menu_mylocation));
		}
		
		if (getTipo().contentEquals(TagsEtiquetas.gps)) {//tipo == gps
			this.datos = new PosicionGPS(
					(LocationManager) nav
							.getSystemService(Context.LOCATION_SERVICE));
		} else {// tipo != gps osea posici�n via network

			this.datos = new PosicionNetwork(
					(LocationManager) nav
							.getSystemService(Context.LOCATION_SERVICE));
		}
		escucharEventos();
	}

	/**
	 * M�todo que escucha el evento del bot�n de la etiqueta e inicializa el lanzador
	 */
	public void escucharEventos() {
		this.boton.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				//se abre una canal de comunicaci�n con la capa de gestorTareas-Lanzador de tareas
				lanzador = new Lanzador(datos,etiqueta,sensitive,accuacy);
			}
        });

	}

}
