package com.navegador.gestorEtiquetas.AnalizadorXML;

import java.util.ArrayList;
import android.content.pm.PackageManager;

/**
 *  InfoDisp, informaci�n del dispositvo. Esta clase obtiene informaci�n del dispostivo con el PackageManager 
 *  de una Activity en activa.
 *  Funciones:
 *  	-Lista con los sensores en disponibles
 * @author Guillermo
 *
 */
public class InfoDisp {

	private PackageManager pManager;
	
	public InfoDisp(PackageManager pManager) {
		this.pManager = pManager;
	}
	
	/**
	 * M�todo que obtiene los tags del dispositivo que requieren de hardware instalado en el dispositivo.
	 * Devuelve una lista con los identificadores de los tags. 
	 * A trav�s del PackageManager se obtienen los sensores hardware disponibles.
	 * 
	 * @return lista
	 */
	public ArrayList tagsDisp() {
		ArrayList lista = new ArrayList();
		if (pManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
			lista.add(TagsEtiquetas.camara);
			lista.add(TagsEtiquetas.codigoQR);
			lista.add(TagsEtiquetas.oVirtual);
		}

		if (pManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)
				&& pManager.hasSystemFeature(PackageManager.FEATURE_LOCATION)) {
			
		}

		if (pManager.hasSystemFeature(PackageManager.FEATURE_LOCATION_NETWORK)
				|| pManager
						.hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS)) {
			lista.add(TagsEtiquetas.posicion);
		}
		if (pManager
				.hasSystemFeature(PackageManager.FEATURE_SENSOR_ACCELEROMETER)) {
			lista.add(TagsEtiquetas.acelerometro);
		}

		if (pManager.hasSystemFeature(PackageManager.FEATURE_NFC)) {
			lista.add(TagsEtiquetas.nfc);
		}

		if (pManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)
				&& pManager
						.hasSystemFeature(PackageManager.FEATURE_CAMERA_AUTOFOCUS)) {
			
		}
		//Combinando la lectura de los campos gravitatorio y magn�tico terrestres proporciona tambi�n informaci�n de orientaci�n
		//el sensor de orientaci�n no hardware.
		if (pManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_COMPASS)) {
			lista.add(TagsEtiquetas.orientacion);
		}
		
		if (pManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_PROXIMITY)) {
			lista.add(TagsEtiquetas.proximidad);
		}
		
		if (pManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_LIGHT)) {
			lista.add(TagsEtiquetas.luminosidad);
		}
		
		return lista;
	}

}
