package com.navegador.gestorEtiquetas.AnalizadorXML;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import  org.w3c.dom.*;
import org.xml.sax.InputSource;
import javax.xml.parsers.DocumentBuilder;  
import javax.xml.parsers.DocumentBuilderFactory;  


/**
 * 
 *	Clase que obtiene y realiza operaciones DOM de una p�gina web. Representa en DOM una web
 *  Funciones:
 *  	-obtener una lista de los tags
 *  	-obtener un String con el c�digo de una p�gina web
 *  	-obtener una lista con los nodos-propiedad de un nodo determinado
 *  
 *  @author Guillermo
 */
public class WebDOM {
	
	private Element elementRaiz;

	/**
	 * Constructor; a partir de la url de una p�gina web genera el documento y elemento raiz DOM.
	 * 
	 * @param link
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public WebDOM(String link) throws SAXException, IOException, ParserConfigurationException{
		Document doc = this.formarDocumento(link);
		
		elementRaiz = doc.getDocumentElement();  
	}
	
	public WebDOM(){
		this.elementRaiz = null;
	}
	
	public Element getRaiz(){
		return elementRaiz;
	}
	
	/**
	 * M�todo recursivo que devuelve una lista con todos los tags del documento encapsulados en Nodos 
	 * 
	 * @param hijos
	 * @return
	 */
	
	public ArrayList obtenerTags(NodeList hijos){
		ArrayList lista = new ArrayList();
		for(int i = 0; i<hijos.getLength();i++){
			Node nodo = hijos.item(i);
			if (nodo instanceof Element){ 
				lista.add(nodo);
				lista.addAll(obtenerTags(nodo.getChildNodes()));
			}
		}
		
		return lista;
	}
	/**
	 * M�todo que devuelve una lista con todos los Nodos que son propiedad de nodo
	 * @param nodo
	 * @return
	 */
	public ArrayList obtenerPropiedades(Node nodo){
		ArrayList lista = new ArrayList();
		
		NamedNodeMap prop = nodo.getAttributes();
		
		for(int i = 0; i<prop.getLength();i++){
			Node n = prop.item(i);
			lista.add(n);
		}
		
		return lista;
	}
	
	
	/**
	 * M�todo que devuelve un documento bien formado con todos las etiquetas html y xml de una p�gina web.
	 * 
	 * @param link
	 * @return Document
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SAXException
	 */
	
	private Document formarDocumento(String link) throws ParserConfigurationException, IOException, SAXException{//Modificando algunas opciones de puede deshabilitar la formaci�n correcta del html y xml?�. Realizar try catha para recojer los excepciones del documentos mal formados
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
		
		InputSource archivo = new InputSource();
		archivo.setCharacterStream(new StringReader(this.codigoWeb(link)));
		
		Document doc = documentBuilder.parse(archivo);
		
		return doc;
	}
	
	
	/**
	 * M�todo que devuelve una cadena de texto con todo el c�digo de una p�gina web.
	 * 
	 * @param link
	 * @return
	 * @throws IOException
	 */
	
	private String codigoWeb(String link) throws IOException{
		URL url = new URL(link);
		BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
		String entrada;
		String cadena="";
		while ((entrada = br.readLine()) != null){
			cadena = cadena + entrada;
		}
		return cadena;
	}
	
}	
