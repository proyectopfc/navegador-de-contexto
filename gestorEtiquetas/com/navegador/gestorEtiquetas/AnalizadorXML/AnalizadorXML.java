package com.navegador.gestorEtiquetas.AnalizadorXML;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import android.content.pm.PackageManager;
/**
 *  Esta clase tiene operaciones con el �rbol DOM y las etiquetas, analiza las etiquetas v�lidas 
 *  y sus par�metros de configuraci�n del �rbol DOM formado por el xml.
 * @author Guillermo
 *
 */
public class AnalizadorXML { 
		
	private WebDOM webDOM;
	private InfoDisp infoDisp;//Informaci�n dispositivo
	private ArrayList tagsValidos;
	
	/**
	 * Constructor del objeto que analiza los tags v�lidos finales.
	 * @param url
	 * @param pManager
	 */
	public AnalizadorXML(String url, PackageManager pManager){
		try {
			//Se crea y se inicializa el objeto que representa la p�gina web
			webDOM = new WebDOM(url);
			//Se crea y se inicializa el objeto que obtiene informaci�n del dispositivo
			infoDisp = new InfoDisp(pManager);
			//Obtiene un ArrayList con los tags a representar; entre la intersecci�n de los tags disponibles en el 
			//dispositivo y lo tags que la web
			tagsValidos = tagsValidos(infoDisp.tagsDisp(),webDOM.obtenerTags(webDOM.getRaiz().getChildNodes()));
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Constructor vacio, se utiliza principalmente para obtener las propiedades de un Nodo
	 * 
	 */
	public AnalizadorXML(){
		webDOM = new WebDOM();
		
	}
	
	/**
	 * Obtiene una lista de nodos-atributos. Obtiene las propiedades de una etiqueta.
	 * 
	 * @param nodo
	 * @return
	 */
	public ArrayList getPropiedades(Node nodo){
		ArrayList lista = new ArrayList();
		lista = webDOM.obtenerPropiedades(nodo);
		return lista;
	}
	
	
	public ArrayList getTags(){
		return tagsValidos;
	}
	
	/**
	 * Este m�todo realiza la intersecci�n entre las listas tagsDisp (tags de los que dispone el dispositivo)
	 *  y tagsWeb(tags de la web). Y devuelve una lista con las tags v�lidos.
	 *  El m�todo compara en forma de bucle anidado el de nombre de cada tag(nodo).
	 * 
	 * @param tagsDisp
	 * @param tagsWeb
	 * @return
	 */
	private ArrayList tagsValidos(ArrayList tagsDisp, ArrayList tagsWeb) {
		ArrayList lista = new ArrayList();

		Node nodoWeb = null;

		for (int i = 0; i < tagsWeb.size(); i++) {
			for (int j = 0; j < tagsDisp.size(); j++) {
				nodoWeb = (Node) tagsWeb.get(i);
				if (tagsDisp.get(j).equals(nodoWeb.getNodeName())) {
					lista.add(nodoWeb);
				}
			}
		}
		return lista;
	}
		
		
		
		
		
}
