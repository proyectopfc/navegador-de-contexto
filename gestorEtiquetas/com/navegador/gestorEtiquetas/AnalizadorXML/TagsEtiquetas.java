package com.navegador.gestorEtiquetas.AnalizadorXML;
/**
 * Cadenas de texto que identifican los tags xml
 * @author Guillermo
 *
 */
public class TagsEtiquetas {
	//Posibles Etiquetas
	public static final String camara = "camara"; //<camara> .... </camara>
	public static final String orientacion = "orientacion";
	public static final String acelerometro = "acelerometro";
	public static final String posicion = "posicion";
	public static final String nfc = "nfc";
	public static final String codigoQR = "codigoQR";
	public static final String oVirtual = "oVirtual";
	public static final String luminosidad = "luminosidad";
	public static final String proximidad = "proximidad";
	//Identificadores de etiquetas
	public static final int idCamara = 1;
	public static final int idOrientacion = 2;
	public static final int idAcelerometro = 3;
	public static final int idPosicion = 4;
	public static final int idNFC = 5;
	public static final int idCodigoQR = 6;
	public static final int idoVirtual = 7;
	public static final int idLuminosidad = 9;
	public static final int idProximidad = 10;
	//Configuracion de etiqueta: parámetros 
	public static final String id = "id";
	public static final String timeInterval = "timeInterval";
	public static final String reciever = "reciever";
	public static final String text = "text";
	public static final String x = "x";
	public static final String y = "y";
	public static final String ancho = "ancho";
	public static final String alto = "alto";
	public static final String sensitive = "sensitive";
	public static final String accuacy = "accuacy";
	//Configuración especifica para etiqueta Localizacoón
	public static final String tipo = "tipo";
	//Valor de los Parámetros especificos: Etiqueta Localización
			public static final String gps = "gps"; //tipo=gps
			public static final String net = "net"; //tipo =net
	//Configuración de etiqueta: parámetros especificos
		//Parámetros: Etiqueta Objeto Virtual
		public static final String marca = "marca";
		public static final String url = "url";
	
	
	
}
