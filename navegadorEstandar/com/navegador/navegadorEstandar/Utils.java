package com.navegador.navegadorEstandar;

import java.util.Random;
/**
 * Esta clase implementa m�todos �tiles.
 * @author Guillermo
 *
 */
public class Utils {

	
	/**
	 * M�todo privado que genera un DSK �nico
	 * @return
	 */
	public static String obtenerDSK(){
		return generarDSK(new Random(),"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",20);
	}
	
	/**
	 * M�todo interno que genera un String aleatorio; dado un Random, unos caracteres posibles y un tama�o.
	 * 
	 * @param random
	 * @param caracteres
	 * @param tam
	 * @return
	 */
	private static String generarDSK(Random random, String caracteres, int tam)
	{
	    char[] text = new char[tam];
	    for (int i = 0; i < tam; i++)
	    {
	        text[i] = caracteres.charAt(random.nextInt(caracteres.length()));
	    }
	    return new String(text);
	}
}
