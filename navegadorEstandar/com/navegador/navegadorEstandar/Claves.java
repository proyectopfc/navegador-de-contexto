package com.navegador.navegadorEstandar;

/**
 * Esta clase alamacena las claves para identificar variables de la aplicación que se almacenan en estructuras de datos.
 * @author Guillermo
 *
 */
public final class Claves {

	public final static String claveURL = "url";

}
