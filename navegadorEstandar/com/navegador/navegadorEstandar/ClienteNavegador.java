package com.navegador.navegadorEstandar;

import com.navegador.gestorEtiquetas.GestorEtiquetas;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
/**
 * 
 * Esta clase implementa la funci�n de un cliente de navegador est�ndar del dispositivo android.
 * 
 * El m�todo mas importante es shouldOverrideUrlLoading(WebView view, String url)
 * 
 * @author Guillermo
 *
 */
public class ClienteNavegador extends WebViewClient {
	
	private Navegador navegador;
	private GestorEtiquetas gestor;
	
	public ClienteNavegador(Navegador navegador){
		this.navegador = navegador;
	}
	
	/**
	 * Cada vez que en el navegador se redirige a otra web este m�todo se ejecuta;
	 * visualiza la web y carga el m�dulo de gestor de etiquetas.
	 *  Si el gestor no se ejecuta lanza un mensaje "Web est�ndar"
	 * 
	 */
	public boolean shouldOverrideUrlLoading(WebView view, String url) {
		//formatear url
		navegador.formatoUrl(url);
		// Cargar web
		view.loadUrl(navegador.getUrl());
		// Cargar el gestor de etiquetas
		try {
			gestor = new GestorEtiquetas(navegador);
		} catch (NullPointerException e) {
			// El gestor de etiquetas no se ha inicializado
			Toast.makeText(navegador.getApplicationContext(), "Web est�ndar",
					Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
        
        return true;
    }
	
	

}
