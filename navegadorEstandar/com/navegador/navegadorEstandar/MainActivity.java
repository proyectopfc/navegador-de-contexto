package com.navegador.navegadorEstandar;

import com.navegador.R;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
/**
 * Main de la aplicaci�n, se muestra en pantalla una caja de texto
 * para introducir la url de la aplicaci�n web y un bot�n para iniciar la aplicaci�n web.
 * @author Guillermo
 *
 */
public class MainActivity extends Activity {
	 
	private Intent navegador;
	private Button btBuscar;
	private EditText eTURL;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);      
        
        navegador = new Intent(this, Navegador.class);
        this.btBuscar = (Button)this.findViewById(R.id.btBuscar);
        this.eTURL = (EditText)this.findViewById(R.id.eTURL);

        if(this.internet()){//Conexi�n a internet habilitada
        //Escuchar evento click del boton buscar
        this.escucharEvento(btBuscar);
        this.iniciarbtApp();
    
        }else{
        	Toast.makeText(getApplicationContext(), "No hay conexi�n a internet", Toast.LENGTH_LONG).show();
        }
    }
    
    /**
     * M�todo que hereda de Activiy, finaliza la aplicaci�n por completo. 
     * Este m�todo se activa cuando se presiona la tecla de retroceso en la primera pantalla de la aplicaci�n: Buscador.
     */
    protected void onDestroy(){
		System.runFinalizersOnExit(true);
		System.exit(0);
	}
    
    /**
     * M�todo que escucha el evento ClickListener de un bot�n e inicializa la actividad navegador
     *  pasando la url de la aplicaci�n web. 
     * 
     * @param boton
     */
    private void escucharEvento(Button boton){
    	boton.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				navegador.putExtra(Claves.claveURL, eTURL.getText().toString());
				startActivity(navegador);
			}
        });   
    }
    
    private void escucharEvento(Button boton, final String url){
    	
    	boton.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				navegador.putExtra(Claves.claveURL, url);
				startActivity(navegador);
			}
        });  
    }
  
    
    private void iniciarbtApp(){
    	Button btYT;
    	btYT = (Button)this.findViewById(R.id.btAppYoutube);
    	escucharEvento(btYT,"m.youtube.com/");
    	
    	Button btG;
    	btG = (Button)this.findViewById(R.id.btAppGoogle);
    	escucharEvento(btG,"google.es/");
    	
    	Button btW;
    	btW = (Button)this.findViewById(R.id.btAppWiki);
    	escucharEvento(btW,"es.m.wikipedia.org/");
    	
    	Button btR;
    	btR = (Button)this.findViewById(R.id.btAppRestaurante);
    	escucharEvento(btR,"desolate-springs-9892.herokuapp.com/presentacion/index");
    	
    	Button btD;
    	btD = (Button)this.findViewById(R.id.btAppD);
    	escucharEvento(btD,"pfc.p.ht/appDecoracion/");
    	
    }
    
    /**
     * M�todo interno que comprueba la conexi�n a internet; si el dispositivo esta conectado a internet
     * retorna true, si el dispositivo no esta conectado a internet retorna false.
     * @return
     */
    private boolean internet() {
        ConnectivityManager cm = 
             (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
}
