package com.navegador.navegadorEstandar;

import com.navegador.R;
import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;
import android.webkit.WebView;
/**
 * Actividad Navegador, es la actividad base de la aplicaci�n. Genera el DSK, inicia el cliente del navegador.
 * 
 * @author Guillermo
 *
 */
public class Navegador extends Activity{

	private WebView mWebView;
	private ClienteNavegador cliente;
	private String url;
	private String DSK;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.navegador);
		
		// Strict Mode, se deber�a utilizar hilos de ejecuci�n
		// http://developer.android.com/reference/android/os/StrictMode.html
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		// FIN lineas de c�digo
				
		cliente = new ClienteNavegador(this);
		mWebView = (WebView) findViewById(R.id.webview);
		DSK = Utils.obtenerDSK();
		//Obtener url
		Bundle bundle=getIntent().getExtras();
		//Iniciar aplicaci�n con la url de la web
		this.iniciarCliente("http://"+bundle.getString(Claves.claveURL));
		
	   
	}
	
	public WebView getWebView(){
		return this.mWebView;
	}
	
	public String getUrl(){
		return this.url;
	}
	/**
	 * M�todo que formatea url, prepara la url para una petici�n de tipo get.
	 *  Pasa como par�metro la DSK.
	 * 
	 * @param url
	 */
	public void formatoUrl(String url){
		 this.url = url+"?DSK=" + this.getDSK(); 
	}
	
	private void iniciarCliente(String url){ 
		// Activar JavaScript
		mWebView.getSettings().setJavaScriptEnabled(true);

		// Modifica el cliente del navegador
		mWebView.setWebViewClient(cliente);

		// Permite redirigirse a otras urls
		cliente.shouldOverrideUrlLoading(mWebView, url);
	}
	
	/**
	 * DSK es el identificador �nico de sesi�n y dispositivo que se utiliza como referencia entre
	 * el dispositivo, navegador y servidor.
	 * @return
	 */
	public String getDSK(){
		return DSK;
	}
	
}
